import sbt.Keys.libraryDependencies

name := "spark-simple"

version := "0.1"


scalaVersion := "2.12.12"
val sparkVersion = "2.4.2"

libraryDependencies ++= Seq(
  "io.get-coursier" %% "coursier" % "1.0.3",
  "io.get-coursier" %% "coursier-cache" % "1.0.3",
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.typelevel" %% "cats-core" % "2.0.0",
  "org.scalactic" %% "scalactic" % "3.0.5",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)

assemblyMergeStrategy in assembly := {
  case x if x.endsWith(".class")                       => MergeStrategy.last
  case x if x.endsWith(".properties")                  => MergeStrategy.last
  case x if x.contains("/resources/")                  => MergeStrategy.last
  case x if x.startsWith("META-INF/mailcap")           => MergeStrategy.last
  case x if x.startsWith("META-INF/mimetypes.default") => MergeStrategy.first
  case x if x.startsWith("META-INF/maven/org.slf4j/slf4j-api/pom.") =>
    MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    if (oldStrategy == MergeStrategy.deduplicate)
      MergeStrategy.first
    else
      oldStrategy(x)
}

resolvers += Resolver.mavenLocal
