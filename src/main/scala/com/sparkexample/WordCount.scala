package com.sparkexample

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object WordCount {

  def readFile(fname:String) (implicit spark: SparkSession): RDD[String] = {
    spark.sparkContext.textFile(fname)
  }

  def wordCount(data:RDD[String]): RDD[(String, Int)] =
    data.flatMap(line => line.split(" "))
      .map(word => (word, 1))
      .reduceByKey(_ + _)

}
