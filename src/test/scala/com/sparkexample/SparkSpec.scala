package com.sparkexample
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.{ConstantInputDStream, DStream, InputDStream}
import org.scalatest.FunSpec
import cats.implicits._

import scala.collection.mutable

class SparkSpec extends FunSpec with StreamingSparkSessionWrapper {

  def mergeMaps(m1:Map[String,Int], m2:Map[String,Int]) : Map[String,Int] = {
    m1 combine m2
  }

  describe("reading a text file") {

    it("can merge maps") {
      val m1 = Map("a" -> 2, "b" -> 3)
      val m2 = Map("a" -> 4, "b" -> 5)
      val c = mergeMaps(m1,m2)
      assert(c == Map("a"->6, "b"->8))

    }

    it("we can count the words") {
      val resourcePath: String = getClass.getResource("/james.txt").getPath
      val text = WordCount.readFile(resourcePath)
      val counts: List[(String, Int)] = WordCount.wordCount(text).collect().sortWith{ case ((k1,c1),(k2,c2)) => c2 < c1}.toList.take(10)
      val expected = List(("",39773), ("the",9635), ("of",8197), ("and",5739), ("to",4915), ("in",3635), ("a",3464), ("I",2724), ("is",2486), ("that",2307))
      assert(counts == expected)
    }

    it("we can count the streamed words") {

      var countMap = Map[String,Int]()

      val resourcePath: String = getClass.getResource("/james.txt").getPath

      val stringStream = mutable.Queue.empty[RDD[String]]
      val dataStream: InputDStream[String] = streamingContext.queueStream(stringStream)

      stringStream += WordCount.readFile(resourcePath)

      val stream: DStream[(String, Int)] = dataStream
          .flatMap(_.split(" "))
          .map(x=> (x,1))

      stream.foreachRDD(rdd => {
        val m1: Map[String, Int] = rdd.reduceByKey((a, b) => a+b).collect.toMap
        countMap = mergeMaps(countMap,m1)
      })

      streamingContext.start()
      streamingContext.awaitTerminationOrTimeout(6000)
      streamingContext.stop()

      assert( countMap.get("the") == Some(9635))
    }
  }
}
