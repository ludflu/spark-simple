package com.sparkexample

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.{Seconds, StreamingContext}

trait SparkSessionWrapper {
  implicit lazy val spark : SparkSession = {
    SparkSession.builder().master("local").appName("spark test").getOrCreate()
  }
}
trait StreamingSparkSessionWrapper  extends SparkSessionWrapper{
  private var ssc: StreamingContext = _
  private val master = "local[1]"
  private val appName = "spark-streaming-test"
  private val filePath: String = "target/testfile"
  private val batchDuration = Seconds(1)

  implicit lazy val streamingContext: StreamingContext = {
    val conf = new SparkConf()
      .setMaster(master).setAppName(appName)
    new StreamingContext(spark.sparkContext,batchDuration)
  }
}

