### Spark Simple

This project demonstrates how to write a simple spark job with a unit test

requirements: sbt

installing sbt on osx:
```brew install sbt```

or download from https://www.scala-sbt.org/

to run unit tests:

```
sbt test
[info] SparkSpec:
[info] reading a text file
[info] - can merge maps
[info] - we can count the words
[info] - we can count the streamed words
[info] Run completed in 11 seconds, 612 milliseconds.
[info] Total number of tests run: 3
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 3, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
```
